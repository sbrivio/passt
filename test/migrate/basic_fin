# SPDX-License-Identifier: GPL-2.0-or-later
#
# PASST - Plug A Simple Socket Transport
#  for qemu/UNIX domain socket mode
#
# PASTA - Pack A Subtle Tap Abstraction
#  for network namespace/tap device mode
#
# test/migrate/basic_fin - Outbound traffic across migration, half-closed socket
#
# Copyright (c) 2025 Red Hat GmbH
# Author: Stefano Brivio <sbrivio@redhat.com>

g1tools	ip jq dhclient socat cat
htools	ip jq

set	MAP_HOST4 192.0.2.1
set	MAP_HOST6 2001:db8:9a55::1
set	MAP_NS4 192.0.2.2
set	MAP_NS6 2001:db8:9a55::2

test	Interface name
g1out	IFNAME1 ip -j link show | jq -rM '.[] | select(.link_type == "ether").ifname'
hout	HOST_IFNAME ip -j -4 route show|jq -rM '[.[] | select(.dst == "default").dev] | .[0]'
hout	HOST_IFNAME6 ip -j -6 route show|jq -rM '[.[] | select(.dst == "default").dev] | .[0]'
check	[ -n "__IFNAME1__" ]

test	DHCP: address
guest1	ip link set dev __IFNAME1__ up
guest1	/sbin/dhclient -4 __IFNAME1__
g1out	ADDR1 ip -j -4 addr show|jq -rM '.[] | select(.ifname == "__IFNAME1__").addr_info[0].local'
hout	HOST_ADDR ip -j -4 addr show|jq -rM '.[] | select(.ifname == "__HOST_IFNAME__").addr_info[0].local'
check	[ "__ADDR1__" = "__HOST_ADDR__" ]

test	DHCPv6: address
# Link is up now, wait for DAD to complete
guest1	while ip -j -6 addr show tentative | jq -e '.[].addr_info'; do sleep 0.1; done
guest1	/sbin/dhclient -6 __IFNAME1__
# Wait for DAD to complete on the DHCP address
guest1	while ip -j -6 addr show tentative | jq -e '.[].addr_info'; do sleep 0.1; done
g1out	ADDR1_6 ip -j -6 addr show|jq -rM '[.[] | select(.ifname == "__IFNAME1__").addr_info[] | select(.prefixlen == 128).local] | .[0]'
hout	HOST_ADDR6 ip -j -6 addr show|jq -rM '[.[] | select(.ifname == "__HOST_IFNAME6__").addr_info[] | select(.scope == "global" and .deprecated != true).local] | .[0]'
check	[ "__ADDR1_6__" = "__HOST_ADDR6__" ]

test	TCP/IPv4: guest1, half-close, guest2 > host
g1out	GW1 ip -j -4 route show|jq -rM '.[] | select(.dst == "default").gateway'

hostb	echo FIN | socat TCP4-LISTEN:10006,shut-down STDIO,ignoreeof > __STATESETUP__/msg
#hostb	socat -u TCP4-LISTEN:10006 OPEN:__STATESETUP__/msg,create,trunc

#sleep	20
# Option 1: via spliced path in pasta, namespace to host
# guest1b	{ printf "Hello from guest 1"; sleep 10; printf " and from guest 2\n"; } | socat -u STDIN TCP4:__GW1__:10003
# Option 2: via --map-guest-addr (tap) in pasta, namespace to host
guest1b	{ printf "Hello from guest 1"; sleep 3; printf " and from guest 2\n"; } | socat -u STDIN TCP4:__MAP_HOST4__:10006
sleep	1

mon	echo "migrate tcp:0:20005" | socat -u STDIN UNIX:__STATESETUP__/qemu_1_mon.sock

hostw
hout	MSG cat __STATESETUP__/msg
check	[ "__MSG__" = "Hello from guest 1 and from guest 2" ]
